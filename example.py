import gi

gi.require_versions({'Pango': '1.0', 'PangoAttrCast': '1.0'})
from gi.repository import Pango, PangoAttrCast

size_attr = Pango.attr_size_new(12)
# size_attr is an instance of Pango.Attribute, which does not expose the size.

# Cast it to Pango.AttrSize:
print("Retrieved size:", PangoAttrCast.as_size(size_attr).size)

# If you try to cast to the wrong type, you get None:
assert PangoAttrCast.as_color(size_attr) is None
