# PangoAttrCast

This is a tiny library to make it possible to cast Pango attributes to specific
types through GObject introspection. This is [not yet possible](
https://gitlab.gnome.org/GNOME/pango/-/issues/476) with Pango itself.

Usage from Python:

```python
import gi

gi.require_versions({'Pango': '1.0', 'PangoAttrCast': '1.0'})
from gi.repository import Pango, PangoAttrCast

size_attr = Pango.attr_size_new(12)
# size_attr is an instance of Pango.Attribute, which does not expose the value.

# Cast it to Pango.AttrSize:
print("Retrieved size:", PangoAttrCast.as_size(size_attr).size)
```

There is one such function for each `PangoAttr*` subtype:

* `as_int`
* `as_float`
* `as_string`
* `as_size`
* `as_color`
* `as_fontdesc`
* `as_fontfeatures`
* `as_language`
* `as_shape`

The attribute type is checked, and it returns NULL (None in Python) if you
try to cast to the wrong type.
