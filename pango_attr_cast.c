#include<pango_attr_cast.h>

/**
 * pango_attr_cast_as_int
 * @attr: A PangoAttribute such as weight
 *
 * Returns: (nullable): The attribute as PangoAttrInt, or NULL if it's not an
 * integer attribute.
 */
const PangoAttrInt* pango_attr_cast_as_int(PangoAttribute *attr) {
  switch (attr->klass->type){
    case PANGO_ATTR_STYLE:
    case PANGO_ATTR_WEIGHT:
    case PANGO_ATTR_VARIANT:
    case PANGO_ATTR_STRETCH:
    case PANGO_ATTR_UNDERLINE:
    case PANGO_ATTR_STRIKETHROUGH:
    case PANGO_ATTR_RISE:
    case PANGO_ATTR_FALLBACK:
    case PANGO_ATTR_LETTER_SPACING:
    case PANGO_ATTR_GRAVITY:
    case PANGO_ATTR_GRAVITY_HINT:
    case PANGO_ATTR_FOREGROUND_ALPHA:
    case PANGO_ATTR_BACKGROUND_ALPHA:
    case PANGO_ATTR_ALLOW_BREAKS:
    case PANGO_ATTR_SHOW:
    case PANGO_ATTR_INSERT_HYPHENS:
    case PANGO_ATTR_OVERLINE:
      return (PangoAttrInt *)attr;

    default:
      return NULL;
  }
}

/**
 * pango_attr_cast_as_float
 * @attr: A PangoAttribute such as scale
 *
 * Returns: (nullable): The attribute as PangoAttrFloat, or NULL if it's not a
 * floating point attribute.
 */
const PangoAttrFloat* pango_attr_cast_as_float(PangoAttribute *attr) {
  switch (attr->klass->type){
    case PANGO_ATTR_SCALE:
      return (PangoAttrFloat *)attr;

    default:
      return NULL;
  }
}

/**
 * pango_attr_cast_as_string
 * @attr: A PangoAttribute such as family
 *
 * Returns: (nullable): The attribute as PangoAttrString, or NULL if it's not a
 * string attribute.
 */
const PangoAttrString* pango_attr_cast_as_string(PangoAttribute *attr) {
  switch (attr->klass->type){
    case PANGO_ATTR_FAMILY:
      return (PangoAttrString *)attr;

    default:
      return NULL;
  }
}

/**
 * pango_attr_cast_as_size
 * @attr: A PangoAttribute representing a size
 *
 * Returns: (nullable): The attribute as PangoAttrSize, or NULL if it's not a
 * size attribute.
 */
const PangoAttrSize* pango_attr_cast_as_size(PangoAttribute *attr) {
  switch (attr->klass->type){
    case PANGO_ATTR_SIZE:
    case PANGO_ATTR_ABSOLUTE_SIZE:
      return (PangoAttrSize *)attr;

    default:
      return NULL;
  }
}

/**
 * pango_attr_cast_as_color
 * @attr: A PangoAttribute such as foreground
 *
 * Returns: (nullable): The attribute as PangoAttrColor, or NULL if it's not a
 * color attribute.
 */
const PangoAttrColor* pango_attr_cast_as_color(PangoAttribute *attr) {
  switch (attr->klass->type){
    case PANGO_ATTR_FOREGROUND:
    case PANGO_ATTR_BACKGROUND:
    case PANGO_ATTR_UNDERLINE_COLOR:
    case PANGO_ATTR_STRIKETHROUGH_COLOR:
    case PANGO_ATTR_OVERLINE_COLOR:
      return (PangoAttrColor *)attr;

    default:
      return NULL;
  }
}

/**
 * pango_attr_cast_as_fontdesc
 * @attr: A PangoAttribute representing a font description
 *
 * Returns: (nullable): The attribute as PangoAttrFontDesc, or NULL if it's not
 * a font description attribute.
 */
const PangoAttrFontDesc* pango_attr_cast_as_fontdesc(PangoAttribute *attr) {
  switch (attr->klass->type){
    case PANGO_ATTR_FONT_DESC:
      return (PangoAttrFontDesc *)attr;

    default:
      return NULL;
  }
}

/**
 * pango_attr_cast_as_fontfeatures
 * @attr: A PangoAttribute representing font features
 *
 * Returns: (nullable): The attribute as PangoAttrFontFeatures, or NULL if it's
 * not a font features attribute.
 */
const PangoAttrFontFeatures* pango_attr_cast_as_fontfeatures(PangoAttribute *attr) {
  switch (attr->klass->type){
    case PANGO_ATTR_FONT_FEATURES:
      return (PangoAttrFontFeatures *)attr;

    default:
      return NULL;
  }
}

/**
 * pango_attr_cast_as_language
 * @attr: A PangoAttribute representing a language
 *
 * Returns: (nullable): The attribute as PangoAttrLanguage, or NULL if it's not
 * a language attribute.
 */
const PangoAttrLanguage* pango_attr_cast_as_language(PangoAttribute *attr) {
  switch (attr->klass->type){
    case PANGO_ATTR_LANGUAGE:
      return (PangoAttrLanguage *)attr;

    default:
      return NULL;
  }
}

/**
 * pango_attr_cast_as_shape
 * @attr: A PangoAttribute representing a shape
 *
 * Returns: (nullable): The attribute as PangoAttrShape, or NULL if it's not a
 * shape attribute.
 */
const PangoAttrShape* pango_attr_cast_as_shape(PangoAttribute *attr) {
  switch (attr->klass->type){
    case PANGO_ATTR_SHAPE:
      return (PangoAttrShape *)attr;

    default:
      return NULL;
  }
}
