#include<pango/pango.h>

const PangoAttrInt* pango_attr_cast_as_int(PangoAttribute *attr);
const PangoAttrFloat* pango_attr_cast_as_float(PangoAttribute *attr);
const PangoAttrString* pango_attr_cast_as_string(PangoAttribute *attr);
const PangoAttrSize* pango_attr_cast_as_size(PangoAttribute *attr);
const PangoAttrColor* pango_attr_cast_as_color(PangoAttribute *attr);
const PangoAttrFontDesc* pango_attr_cast_as_fontdesc(PangoAttribute *attr);
const PangoAttrFontFeatures* pango_attr_cast_as_fontfeatures(PangoAttribute *attr);
const PangoAttrLanguage* pango_attr_cast_as_language(PangoAttribute *attr);
const PangoAttrShape* pango_attr_cast_as_shape(PangoAttribute *attr);
